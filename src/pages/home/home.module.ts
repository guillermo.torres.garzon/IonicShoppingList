import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { HomePage } from './home';

@NgModule({
    declarations:[HomePage],
    imports:[IonicPageModule.forChild(HomePage)] //asi logramos el lazy loaded
})

export class HomeModule{

}