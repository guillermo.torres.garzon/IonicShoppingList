import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import { Item } from "../../models/item/item.model";

@Injectable()
export class ShoppingListService{
    //decirle a la base de datos que parte nos interesa
    //private shoppingListRef = 'shopping-list'
    private shoppingListRef = this.db.list<Item>('shopping-list');

    constructor(private db:AngularFireDatabase){}

    getShoppingList(){
        console.log('getShoppingList');
        return this.shoppingListRef;
    }

    addItem(item:Item){
        return this.shoppingListRef.push(item);
    }

    editItem(item:any){
        console.log(item);
        return this.shoppingListRef.update(item.key, item);

    }

    removeItem(item:any)
    {
        return this.shoppingListRef.remove(item.key);
    }
}

/*
Configurar Firebase Database
{
  "rules": {
    ".read": true,
    ".write": true
  }
}
*/